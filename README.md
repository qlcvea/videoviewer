# videoviewer

A bad tool to show videos

---

This is a very simple Electron app to show a video that is slightly more
"professional-looking" than a video player window being dragged around.  
With this, the viewing can be prepared by only dragging a window and making the
<video> element inside it fullscreen (by clicking anywhere inside the window)
and then the playback can be controlled with the other window created when the
program is started.

To install dependencies: `npm install`  
To run: `npm start`
