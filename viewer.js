const { ipcRenderer } = require('electron');
var playPause = () => {
  if (document.getElementById('videoPlayer').paused) {
    document.getElementById('videoPlayer').play();
  } else {
    document.getElementById('videoPlayer').pause();
  }
}
var play = (url, loop) => {
  document.getElementById('videoPlayer').loop = loop;
  document.getElementById('videoPlayer').pause();
  document.getElementById('videoPlayer').src = url;
  document.getElementById('videoPlayer').currentTime = 0;
  document.getElementById('videoPlayer').play();
};
window.addEventListener('load', () => {
  document.getElementById('videoPlayer').addEventListener('ended', () => {
    if (!document.getElementById('videoPlayer').loop) ipcRenderer.send('main-command', "goToLoop()");
  });
  document.getElementById('videoPlayer').addEventListener('timeupdate', () => {
    ipcRenderer.send('main-command', "document.getElementById('currentTime').innerHTML='" + document.getElementById('videoPlayer').currentTime + "'");
  });
  document.getElementById('videoPlayer').addEventListener('durationchange', () => {
    ipcRenderer.send('main-command', "document.getElementById('totalTime').innerHTML='" + document.getElementById('videoPlayer').duration + "'");
  });
});
