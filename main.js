const { remote, ipcRenderer } = require('electron');
const fileUrl = require('file-url');
const fs = require('fs');
var maxBtn = null, grabbingButton = false;
var playPause = () => {
  ipcRenderer.send('viewer-command', "playPause()");
};
var goToLoop = () => {
  ipcRenderer.send('viewer-command', "play('" + document.getElementById('defaultLoop').value.replace(/'/g, '\\\'') + "',true)");
};
var darkChanged = () => {
  ipcRenderer.send('viewer-dark', document.getElementById('dark').checked);
  if (document.getElementById('dark').checked) {
    ipcRenderer.send('viewer-command', "document.getElementById('help').className='hidden'");
  } else {
    ipcRenderer.send('viewer-command', "document.getElementById('help').className=''");
  }
};
var goTime = () => {
  ipcRenderer.send('viewer-command', "document.getElementById('videoPlayer').currentTime=" + document.getElementById('timeSel').value);
};
var selectDefaultLoop = () => {
  document.getElementById('defaultLoop').value = fileUrl(
    remote.dialog.showOpenDialog({ properties: ['openFile'] })[0]
  );
};
var selectVideo = () => {
  var video = fileUrl(
    remote.dialog.showOpenDialog({ properties: ['openFile'] })[0]
  );
  var loop = document.getElementById('loop').checked.toString();
  ipcRenderer.send('viewer-command', "play('" + video.replace(/'/g, '\\\'') + "'," + loop + ")");
  document.getElementById('loop').checked = false;
};
var genBtns = () => {
  document.getElementById('buttonGen').style.display = 'none';
  document.getElementById('buttons').innerHTML = '';
  maxBtn = document.getElementById('buttonGenN').value - 1;
  var bind = (i) => {
    return function() {
      document.body.style.display = 'none';
      grabbingButton = i;
    }
  }
  var video = (i) => {
    return function() {
      document.getElementById('video-' + i).value = fileUrl(
        remote.dialog.showOpenDialog({ properties: ['openFile'] })[0]
      );
    }
  }
  for (var i = 0; i <= maxBtn; i++) {
    var bindBox = document.createElement('input');
    bindBox.type = 'number';
    bindBox.id = 'bind-' + i;
    bindBox.size = "2";
    bindBox.readOnly = true;
    var bindBtn = document.createElement('button');
    bindBtn.innerHTML = 'bind';
    bindBtn.addEventListener('click', bind(i));
    var videoBox = document.createElement('input');
    videoBox.type = 'text';
    videoBox.id = 'video-' + i;
    videoBox.size = "50";
    videoBox.readOnly = true;
    var videoBtn = document.createElement('button');
    videoBtn.innerHTML = 'select';
    videoBtn.addEventListener('click', video(i));
    var loopBox = document.createElement('input');
    loopBox.type = 'checkbox';
    loopBox.id = 'loop-' + i;
    var loopLabel = document.createElement('label');
    loopLabel.for = 'loop-' + i;
    loopLabel.innerHTML = ' loop';
    var div = document.createElement('div');
    div.appendChild(bindBox);
    div.append(' ');
    div.appendChild(bindBtn);
    div.append(' ');
    div.appendChild(videoBox);
    div.append(' ');
    div.appendChild(videoBtn);
    div.append(' ');
    div.appendChild(loopBox);
    div.appendChild(loopLabel);
    document.getElementById('buttons').appendChild(div);
  }
}
var importData = () => {
  var path = remote.dialog.showOpenDialog({
    properties: ['openFile'],
    filters: [
      {
        name: 'JSON file',
        extensions: [
          'json'
        ]
      }
    ]
  })[0];
  fs.readFile(path, (err, data) => {
    if (err) {
      remote.dialog.showMessageBox({
        type: 'error',
        title: 'error',
        message: 'an error occurred while opening the config file.',
        detail: err.toString()
      });
      return;
    }
    try {
      data = JSON.parse(data);
    } catch (e) {
      remote.dialog.showMessageBox({
        type: 'error',
        title: 'error',
        message: 'an error occurred while opening the config file.',
        detail: err.toString()
      });
      return;
    }
    if (typeof data.defaultLoop == 'string') {
      document.getElementById('defaultLoop').value = data.defaultLoop;
    }
    if (typeof data.dark == 'boolean') {
      document.getElementById('dark').checked = data.dark;
      darkChanged();
    }
    if (typeof data.binds == 'object') {
      if (typeof data.binds.length == 'number') {
        document.getElementById('buttonGenN').value = data.binds.length;
        genBtns();
        for (var i in data.binds) {
          console.log(i, data.binds[i]);
          if (typeof data.binds[i].bind == 'number') {
            document.getElementById('bind-' + i).value = data.binds[i].bind;
          }
          if (typeof data.binds[i].url == 'string') {
            document.getElementById('video-' + i).value = data.binds[i].url;
          }
          if (typeof data.binds[i].loop == 'boolean') {
            document.getElementById('loop-' + i).checked = data.binds[i].loop;
          }
        }
      }
    }
  })
}
var exportData = () => {
  remote.dialog.showMessageBox(
    {
      type: 'question',
      message: 'export default loop video?',
      buttons: [
        'yes',
        'no',
        'cancel'
      ],
      cancelId: 2
    },
    (response1, _checkboxChecked) => {
      console.log(response1);
      if (response1 == 2) {
        return;
      }
      remote.dialog.showMessageBox(
        {
          type: 'question',
          message: 'export "professional-mode" state?',
          buttons: [
            'yes',
            'no',
            'cancel'
          ],
          cancelId: 2
        },
        (response2, _checkboxChecked) => {
          if (response2 == 2) {
            return;
          }
          remote.dialog.showMessageBox(
            {
              type: 'question',
              message: 'export keybinds?',
              buttons: [
                'yes',
                'no',
                'cancel'
              ],
              cancelId: 2
            },
            (response3, _checkboxChecked) => {
              if (response3 == 2) {
                return;
              }
              if (maxBtn === null) {
                response3 = 1;
              }
              var data = {};
              if (response1 == 0) {
                data.defaultLoop = document.getElementById('defaultLoop').value;
              }
              if (response2 == 0) {
                data.dark = document.getElementById('dark').checked;
              }
              if (response3 == 0) {
                data.binds = [];
                for (var i = 0; i <= maxBtn; i++) {
                  data.binds[i] = {
                    bind: Number(document.getElementById('bind-' + i).value),
                    url: document.getElementById('video-' + i).value,
                    loop: document.getElementById('loop-' + i).checked
                  }
                }
              }
              var text = JSON.stringify(data);
              var path = remote.dialog.showSaveDialog({
                filters: [
                  {
                    name: 'JSON file',
                    extensions: [
                      'json'
                    ]
                  }
                ]
              });
              fs.writeFile(path, text, (err) => {
                if (err) {
                  remote.dialog.showMessageBox({
                    type: 'error',
                    title: 'error',
                    message: 'an error occurred while writing the config file.',
                    detail: err.toString()
                  });
                }
              });
            }
          );
        }
      );
    }
  );
}
window.addEventListener('keydown', (e) => {
  if (grabbingButton === false && maxBtn !== null) {
    for (var i = 0; i <= maxBtn; i++) {
      if (document.getElementById('bind-' + i).value == e.keyCode) {
        var video = document.getElementById('video-' + i).value;
        var loop = document.getElementById('loop-' + i).checked.toString();
        ipcRenderer.send('viewer-command', "play('" + video.replace(/'/g, '\\\'') + "'," + loop + ")");
      }
    }
  } else if (maxBtn !== null) {
    document.body.style.display = '';
    document.getElementById('bind-' + grabbingButton).value = e.keyCode;
    grabbingButton = false;
  }
});
