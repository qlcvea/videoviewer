const { app, ipcMain, BrowserWindow, Menu } = require('electron');
const package = require('./package.json');

let mainWindow, viewerWindow;

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    title: package.productName,
    width: 800,
    height: 600,
    fullscreenable: false,
    webPreferences: {
      nodeIntegration: true
    }
  });
  mainWindow.loadFile('main.html');
  //mainWindow.webContents.openDevTools();
  mainWindow.on('closed', () => {
    app.quit();
  });
  viewerWindow = new BrowserWindow({
    title: package.productName + ' - video player',
    width: 300,
    height: 300,
    maximizable: false,
    resizable: false,
    webPreferences: {
      nodeIntegration: true
    }
  });
  viewerWindow.loadFile('viewer.html');
  viewerWindow.on('closed', () => {
    app.quit();
  });
  Menu.setApplicationMenu(Menu.buildFromTemplate([
    {
      label: package.productName,
      submenu: [
        { role: 'quit' }
      ]
    }
  ]));
  if (process.platform !== 'darwin') {
    viewerWindow.setMenu(null);
  }
});

app.on('window-all-closed', () => {
  app.quit();
});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  }
});

ipcMain.on('viewer-command', function(_event, data) {
  viewerWindow.webContents.executeJavaScript(data);
});

ipcMain.on('main-command', function(_event, data) {
  mainWindow.webContents.executeJavaScript(data);
});

ipcMain.on('viewer-dark', function(_event, data) {
  if (data) {
    viewerWindow.setTitle('');
  } else {
    viewerWindow.setTitle(package.productName + ' - video player');
  }
});
